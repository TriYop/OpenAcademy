# Authentification LDAP sur Red Hat 6


## Configuration de notre annuaire
Dans tout ce tutoriel, nous supposerons que nous avons un 
annuaire LDAP configuré pour écouter sur le port 636 en LDAP / TLS.

### DIT

Notre annuaire est organisé comme suit : 
- `ou=groups`			l'ensemble des groupes système partagés se trouve ici
- `ou=users`			l'ensemble des utilisateurs systle partagés se trouve ici
  - `ou=app`			les utilisateurs techniques (applicatifs) 
  - `ou=adm`			les administrateurs de machines
- `ou=hosts`			les machines
  - `ou=Qualification`		les machines de l'environnement de qualification / recette
  - `ou=Production`		les machines de l'environnement de production
  
### Schéma

#### Utilisateurs
Les objets placés dans la branche ̀`ou=adm,ou=users,ou=System` sont les seuls utilisateurs autorisés à ouvrir une session 
interactive sur les serveurs.

| Attribut | Valeur (exemple) | Description |
| ----- | ----- | -----|
| objectClass | InetOrgPerson PosixAccount ShadowAccount | Type d'objet dans l'annuaire |
| sn | (DOE) | Nom de famille |
| givenName | (John) | Prénom |
| cn | (John DOE) | Nom complèt |
| uid | (jd12345) | Login de l'utilisateur |
| userPassword | (mot2P@sse) | Mot de passe de l'utilisateur. L'annuaire peut en gérer la politique de complecxité et de renouvellement). Il est stocké hashé |
| uidNumber | (12345) | Numéro unique d'utilisateur |
| gidNumber | (1000) | Numéro du groupe principal de l'utilisateur, |
| loginShell | (/bin/sh) | Shell unix de l'utilissateur |
| homeDirectory | (/home/jd12345) | Répertoire home de l'utilisateur. |
| mail | (john.doe@example.com) | Adresse email de l'utilisateur |

 #### Groupes
 Les objets placés dans la branche `ou=groups,ou=System` sont les groupes d'utilisateurs
 et partagent les attributs décrits ci-dessous
 
 | Attribut | Valeur (exemple) | Description |
| ----- | ----- | -----|
| cn | (SystemOperator) | Nom du groupe |
| objectClass | GroupOfUniqueNames posixGroup | Type d'objet |
| gidNumber | (1000) | identifiant numérique du groupe |
| uniqueMember | uid=jd12345,ou=adm,ou=users,ou=System | Liste des DN des utilisateurs membres du groupe |

#### Machines
Les objets représentant les machines sont stockés dans la branche `ou=hosts, ou=System`.

| Attribut | Valeur (exemple) | Description |
| ----- | ----- | -----|
| cn | (pc129) | nom de la machine. Plusieurs valeurs peuvent être appliquées mais une seule sera utilisée dans le DN. |
| objectClass | ipHost device | Type d'objet dans l'annuaire |
| ipHostNumber | (192.168.18.129) | Adresse IP de la machine |
| description | ... | description de la machine |

## Installation de la machine client LDAP
### Installation des prérequis

    #> yum install sssd
    #> authconfig --enablesssd --enablelocauthorize --disablefingerprint --update
    
Editer le fichier de configuration de SSSD  : `/etc/sssd/sssd.conf`

     1. [domain/default]
     2. ldap_id_use_start_tls = False
     3. ldap_tls_cacertdir = /etc/openldap/cacerts
     4. ldap_tls_reqcert = allow
     5. ldap_network_timeout = 3
     6. ldap_uri = ldaps://ldap1.extranet.cartes-bancaires.com:636/, ldaps://ldap2.extranet.cartes-bancaires.com:636/, ldaps://ldap3.extranet.cartes-bancaires.com:636/
     7. ldap_search_base = ou=System,dc=interne,dc=cartes-bancaires,dc=com
     8. ldap_access_filter = (&(objectclass=shadowaccount)(objectclass=posixaccount))
     9. ldap_schema = rfc2307bis
    10. ldap_user_search_base = ou=Users,ou=System,dc=interne,dc=cartes-bancaires,dc=com
    11. ldap_group_search_base = ou=Groups,ou=System,dc=interne,dc=cartes-bancaires,dc=com
    12. ldap_group_member = uniqueMember
    13. ldap_group_nesting_level = 3
    14. id_provider = ldap
    15. auth_provider = ldap
    16. access_provider = ldap
    17. chpass_provider = none
    18. cache_credentials = True
    19. entry_cache_timeout = 600
    20. default_shell = /bin/bash
    21. create_homedir = True
    22. skel_dir = /etc/skel
    23. 
    24. [sssd]
    25. services = nss, pam
    26. config_file_version = 2
    27. domains = default
    28. 
    29. [nss]
    30. filter_users = root,ldap,named,avahi,haldaemon,dbus,radiusd,news,nscd
    31. 
    32. [pam]
    33. 
    34. [sudo]
    35. 
    36. [autofs]
    37. 
    38. [ssh]

Pour que le service démarre, il faut limiter les droits sur le fichier de configuration
    
    #> chmod 0600 /etc/sssd/sssd.conf
    #> /etc/init.d/sssd restart

Une fois le service redémarré, pour vérifier la configuration, il est possible d'interroger le service 

    #> getent group SystemOperator
    #> getent passwd jd12345

On peut enfin vérifier que l'authentification est bien configurée
en testant d'ouvrir une session sur un utilisateur LDAP : 

    #> su - jd12345
    $> ^D
    #> su - nobody -s /bin/bash -c `su - jd12345`
    
### Configurer le serveur SSH

Editer le fichier `/etc/ssh/sshd_config` et s'assurer de la présence de la ligne suivante :
    
    1. UsePAM yes
    
Redémarrer le serveur ssh pour appliquer le changement de la configuration

    #> service sshd restart
    
Se connecter au serveur par ssh depuis une autre machine pour valider le fonctionnement:

    $> ssh jd12345@$HOST
    
saisir le mot de passe de l'utilisateur au prompt
Si tout fonctionne, sécuriser l'accès SSH en interdisant le login root et le login sans mot de passe.

> ATTENTION : l'utilisation de la directive AllowGroups ne doit pas préciser uniquement un groupe local si les utilisateurs LDAP doivent pouvoir ouvrir une session SSH. Il est préférable de préciser, si besoin, le groupe SystemOperator qui regroupe l'ensemble des utilisateurs du LDAP possédant des pricvilèges d'administration des serveurs.

### Configurer le démarrage des services

Configurer les services comme suit :

    #> chkconfig nslcd --levels 123456 off
    #> chkconfig nscd --levels 123456 off
    #> chkconfig sssd --levels 345 on
    #> chkconfig oddjob --levels 345 on  
    
## Configuration des privilèges

Les privilèges des utilisateurs sont déclarés dans l'annuaire au niveau de groupes LDAP de type `posixGroup` dans la branche 
`ou=Groups,ou=System`. Les groupes sont stockés « à plat » dans la branche mais sont organisés de manière logique. 
Les utilisateurs ne doivent être membres que des groupes feuille.

| Groupe | Utilisation |
|-----|-----|
| `Agp-app-apache` | administration d'apache |
| `Agp-app-tomcat` | administration de tomcat |
| `Agp-app-glassfish` | administration de glassfish |
| `Agp-app-dba-mysql` | administration de MySQL |
| `Agp-app-dba-postgresql` | administration de PostgreSQL |
| `Agp-SYS` | administration du système |
| `SystemOperator` | Ouverture de session interactive |

Les privilèges des utilisateurs sont gérés par sudo et donc configurés au travers du fichier `/etc/sudoers` (éditable avec `visudo`). Les privilèges sont accordés au groupe sur un ensemble d'applications. 

    # Les alias ci-dessous doivent être adaptés aux services installés sur la machine.
    Cmnd_Alias APACHE = /etc/init.d/httpd
    Cmnd_Alias TOMCAT = /etc/init.d/tomcat
    Cmnd_Alias GLASSFISH = /etc/init.d/glassfish
    Cmnd_Alias MYSQL = /etc/init.d/mysql
    Cmnd_Alias POSTGRESQL = /etc/init.d/postgresql
    
    %agp-app-apache		ALL=APACHE
    %agp-app-tomcat		ALL=TOMCAT
    %agp-app-glassfish	ALL=GLASSFISH
    %agp-dba-mysql		ALL=MYSQL
    %agp-dba-postgresql	ALL=POSTGRESQL
    %agp-SYS			ALL=ALL


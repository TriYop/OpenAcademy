#!/usr/bin/env bash
#

function build_slideware() {
    dir=$(dirname $1)
    file=$(basename $1)
    cd ${dir}
    # Let's now build tex file
    pdflatex $file | tee -a $buildLog
}

rootDir=$(readlink -e $0 | xargs -i dirname {})
buildLog=${rootDir}/build.log
echo. > $buildLog
for file in $(find . -name '*.tex'); do
  texFile=$( echo ${rootDir}/${file} | xargs -i readlink -e {} )
  build_slideware "${texFile}"
done

# *DevOps Security Guide*
# How to move from DevOps to DevSecOps

## Introduction
### What is DevOps ?

DevOps is a phylosophy, that can be leveraged to manage IT product lifecycle.
Its purpose is to tighten the links between the Devs (Developers) and Ops (Operations) in order to speed up product deployment to production. 
As it relies on agile principles, it is often described as the "Agile Methodology for ops".

Alike Agile methodologies for software development, DevOps comes with a bunch of commonly used tools such as continuous deployment. While those tools are a great way to achieve some of DevOps goals, it is necessary to remember that they are only a small part of the whole equation (remember the 1st value of the Agile manifesto: _Individual and Interactions over Processes and **Tools**_). 

> DEVOPS IS NOT CONTINUOUS DEPLOYMENT, neither is it NOOPS.

DevOps wants Dev and Ops to collaborate, to work together at a shared goal which is customer satisfaction. This requires communication between them (_enable face-to-face interactions_ principle), and to break the cultural walls between them which may happen through the "_Support, trust and motivate the people involved_" principle.

DevOps needs the team to *self-organize* and to *self-improve* continuously to ease the reach of the objectives. Choosing the right tools and automating the right tasks is then a way of improving the way of working leading to those objectives. The **whole team** will be held **responsible** for what it delivers to production. There should not be anymore "_your code is so buggy that it makes my server crash_"

DevSecOps aims at not hearing anymore security guys howl "_WTF_" on every single line of code, dev and ops cry after every pentest which bypassed the infrastructure security mechanisms put into place that were bought by the CISO to enforce security regulatory constraints.




\documentclass[usepdftitle=true,handout]{beamer}

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{lmodern}
\usepackage[frenchb]{babel}
\usepackage[T1]{fontenc}

\usetheme{Berkeley}
\usecolortheme{beetle}
\usefonttheme{structurebold}


\begin{document}

    \title {DevOps Security Best Practices}
    \author{Yvan JANET}
    \institute{dev@yvanjanet.net}
    \date{\today}

    \setbeamertemplate{frametitlecontinuation}{\insertcontinuationcount}

    \begin{frame}[plain]
        \titlepage
    \end{frame}

    \begin{frame}[plain]
        \frametitle{Table of contents}
        % \tableofcontents[pausesections,hideallsubsections]
        \tableofcontents[hideallsubsections]
    \end{frame}

    \AtBeginSection[]
    {
    \begin{frame}[plain]
        \tableofcontents[currentsection,hideothersubsections]
    \end{frame}
    }

    \begin{frame}[plain]
        \frametitle{Summary}
        This presentation will deal with DevOps and security concerns:
        \begin{itemize}
            \item{those it can address}
            \item{those it creates}
        \end{itemize}
    \end{frame}



    \section{Culture and Process}
    % ---------------------------------------------------------------------------------------
    % Culture and process related
    % ---------------------------------------------------------------------------------------

    \subsection{DevOps culture}

    \begin{frame}
        \frametitle{Team Responsability}
        Security is not a matter of "if" but of "when".
        \begin{verse}
            "If anything can go wrong, it will." (A. MURPHY)
        \end{verse}

        Both Devs and Ops must be ready to address the breach event.
        The action plan must be relevant according to the evaluated risks.
    \end{frame}


    \begin{frame}
        \frametitle{Understand the risk}

        The cost of breaches is drastically increasing and security should be taken seriously inside an organization.

        DevOps engineers should play an important role in advocating for better security practices.
    \end{frame}


    \begin{frame}
        \frametitle{Gamify security training}
        Humans are the weakest links in the security chain but still are at the heart of DevOps.

        DevOps should contribute to the security awareness of all the employees in a company.

        By explaining how an attacker could infiltrate your company, you will increase the awareness and thus minimize
        the probability and the impact of a successful hack.

        \begin{exampleblock}{REMINDER}
            Don't forget social engineering attacks such as fishing or spear-fishing.
        \end{exampleblock}

    \end{frame}



    \begin{frame}
        \frametitle{Stay on top of best practices}
        DevOps is an ever-changing landscape.


        Ensure to stay up to date in terms of new technologies, vulnerabilities and best practices.

        \begin{alertblock}{CAUTION}
            Always using the newest and hypest technology is not always a good choice in terms of security.
            A bulletproof technology might be a better choice depending on the impediments of your products.
        \end{alertblock}
    \end{frame}





    \subsection{Identity and Access Governance}

    \begin{frame}
        \frametitle{Detect insider threats}
        Monitor your users and practice strong Identity and Access Management policy, especially for privileged accounts.

        \begin{alertblock}{CAUTION}
            The more power a user has, the more you should know how he uses it.
        \end{alertblock}

        Monitor users behaviour to detect attackers before an attack happens
    \end{frame}

    \begin{frame}
        \frametitle{Repeatable process for HR movements}
        Ensure a repeatable process for onboarding and offboarding employees, contractors, etc...

        This may be done through a simple checklist or a more automated workflow. Position change should follow a similarly
        predefined and traced process.
    \end{frame}

    \begin{frame}
        \frametitle{Monitor your authorizations}
        Keep track of authorization changes in your production.

        Regularly review traces, not only in case of an incident post-mortem.
    \end{frame}


    \section{Code}
    % ---------------------------------------------------------------------------------------
    % Application code and development practices
    % ---------------------------------------------------------------------------------------
    \subsection{Coding practices}

    \begin{frame}
        \frametitle{Keep your dependencies up to date}
        Third party libraries can put your application at risk.

        \begin{itemize}
            \item Make sure you track your vulnerable packages and update them regularly.
            \item Ensure used packages are still actively maintained.
        \end{itemize}

        \begin{alertblock}{CAUTION}
            It is a very bad practice to have strong coupling to a precise library version (foo.jar release 1.3.22.1887).
        \end{alertblock}
    \end{frame}

    \begin{frame}
        \frametitle{Monitor third party vendors}
        Just like libraries, third party vendors may be subject to vulnerabilities or be hacked.
        Relying on a hacked third party service may engender risks to your app as well.
    \end{frame}

    \begin{frame}
        \frametitle{Build reproductability}
        Two builds of the same codebase should both produce functionally similar artifacts or fail

        A dependancy upgrade should guarantee no functional change to the application. Dependency manager should be chosen
        consequently.
    \end{frame}


    \begin{frame}
        \frametitle{Never implement your own crypto !}
        The problem with cryptography is, that you don't know you are at risk until your crypto has been hacked.

        Standards that are widely used are also widely tested and weaknesses may be identified before you get hacked so you still can switch to less vulnerable one.
        Using your own crypto, you would know you are vulnerable once you are already hacked.

        So don't do your own crypto. Use standards instead!
    \end{frame}



    \begin{frame}[fragile]
        \frametitle{Ensure you are using security headers}
        Modern browsers support a set of headers dedicated to block certain types of attacks.

        \begin{verbatim}
            X-Frame-Options
            Strict-Transport-Security
            Referer-Policy
        \end{verbatim}

        Make sure you properly imlplemented all security headers.
        Don't forget about the CSP.
    \end{frame}


    \subsection{Application design}
    \begin{frame}
        \frametitle{Don't store sensitive data}
        Avoid storing sensitive data whenever it is not absolutely required

        Stored data is ALWAYS easier to retrieve than the same data in volatile memory.


    \end{frame}

    \begin{frame}
        \frametitle{ Do not trust Biometrics }
        Biometrics are not for authentication. they define your identity but do not prove it as they are not secret.

        \begin{itemize}
            \item Your fingerprint is yours only but is not secret (you leave them everywhere you go).
            \item All you can \underline{prove} is that the fingerprint (or maybe the finger itself) provided to the system is yours, not that \underline{you} are trying to connect!
        \end{itemize}

    \end{frame}

    \begin{frame}

        \begin{theorem}[Authentication factor]
            An authentication factor does not have to be unique but
            \begin{itemize}
                \item to be kept secret
                \item to be independant from the subject
		\item to be independant from the system
                \item to be distinct from other authentication factors
            \end{itemize}
        \end{theorem}
        Biometrics violate the two first items, that is why it cannot be trusted as authentication factor.
    \end{frame}



    \begin{frame}
        \frametitle{Enforce two-factor authentication (2FA)}

        Whenever possible, use 2FA to authenticate users.
        Strong authentication mecanism is the best way to avoid most social engineering based attacks.


        Available factors are
        \begin{itemize}
            \item password / passphrase / pin code : what you know
            \item  an OTP (One Time Password), a LTP (limited Time Pass => Google Authenticator)
            \item a smartcard, a smart token, .. (Yubikeys, RSA SecurID, ...)
            % \item your behaviour : what you can do
        \end{itemize}

        Use 2 SV (2 steps verification) as an enlightened 2FA with two factors of the same kind or with system dependencies (ex: multiple passphrases, ...)
    \end{frame}



    \begin{frame}
        \frametitle{Protect your application against common breaches}
        Protect your application against common attacks (see OWASP top 10) since design and coding.
    \end{frame}

    \begin{frame}
        \frametitle{Protect your users against account takeovers}
        Brute force attacks are easy to setup. You should make sure your users are protected against account takeovers.
        => Lockout policy is to be set up.
    \end{frame}



    % Automate security checks
    \subsection{Continuous integration practices}

    \begin{frame}
        \frametitle{Protect your toolchain like your product}
        Your continuous deployment pipeline is the backbone of your IT and probably its most vulnerable entry point.

        Security should be checked at each step.
        Your CI builds should fail if you detect a security vulnerability.

        Store your CI configuration for traceability and audit.
    \end{frame}



    \begin{frame}
        \frametitle{Analyze your code for vulnerabilities}
        Static Application Security Testing (SAST) is an easy and fast way to find security vulnerabilities in your code.

        You can enforce SAST security checks in your CI but be aware of the high number of false positives that can frustrate your developers.
    \end{frame}



    \begin{frame}
        \frametitle{Analyze your application for vulnerabilities}
        Integrate a Dynamic Application Security Testing (DAST) tool in your CI, but just like SAST, be aware of the high number of false positives.

        Some sample tools :
        \begin{itemize}
            \item Arachni scanner
            \item OWASP ZAP
            \item Acunetix Vulnerability Scanner
        \end{itemize}
    \end{frame}



    % Go further with security checks
    \begin{frame}
        \frametitle{Go hack yourself}
        If your company doesn't have yet a structured security team, help create a multidisciplinary Red Team to stress your application and infrastructure.

        Providing an easy environment for the Red Team to attack the application should be part of the scope of DevOps.
    \end{frame}





    \section{Infrastructure}
    \subsection{Automate}
    \begin{frame}
        \frametitle{Automatically configure \& update your servers}
    \end{frame}

    \begin{frame}
        \frametitle{Upgrade your servers regularly}
        Server packages and libraries are often updated when security vulnerabilities are found.

        You should update them as soon as a security vulnerability is found.
    \end{frame}

    \begin{frame}
        \frametitle{Use an immutable infrastructure}
        Use immutable infrastructures to avoid having to manage and update your servers.
        You should only have to build and deploy newer ones to replace previous ones.
    \end{frame}

    \begin{frame}
        \frametitle{Backup regularly}
	
	Who knows when a breach will be discovered on your server. To prevent
	data loss, your server needs a backup strategy fitting your needs.
	    \begin{alertblock}{WARNING}
		Backup without restore test is worse than no backup : if you believe 
		to be safe, you will most probably discover the issue the day you won't be able
		to restore data from a backup done before a real crash.
	    \end{alertblock}
    \end{frame}

    \subsection{Monitoring}
    \begin{frame} % Infra /
        \frametitle{Audit your infrastructure on a regular basis}
	    Keep track of your assets and check if they are still needed, and still up to date.
	    \begin{itemize}
		    \item Do what is necessary to renew certs before they expire. 
		    \item Just like TLS certificates, DNS records may expire. Automate renewal to avoid domain takeover.
	    \end{itemize}
    \end{frame}



    \begin{frame}
        \frametitle{Get notified when your app is under attack}
        You will be attacked, that is a fact. Ensure that your monitoring system will detect security events and trigger appropriate notice
        actions before it is too late.

    \end{frame}

    \subsection{Encryption}


    \begin{frame}
        \frametitle{Encrypt all the things}
    \end{frame}

    \begin{frame}
        \frametitle{Check your SSL / TLS Configurations}
    \end{frame}



    \begin{frame}
        \frametitle{Manage secrets with dedicated tools \& vaults}
    \end{frame}

    \begin{frame}
        \frametitle{store encrypted passwords in your configuration management}
    \end{frame}


    \subsection{Track and control}

    \begin{frame}
        \frametitle{Harden configurations}
    \end{frame}

    \begin{frame}
        \frametitle{Control access on your cloud providers}
    \end{frame}

    \begin{frame}
        \frametitle{Keep your containers protected}
    \end{frame}

    \begin{frame}
        \frametitle{Log all the things}
    \end{frame}




    \begin{frame}
        \frametitle{Protect against Denial of Service (DoS)}
	There is not much to do against DoS attack, especially when they are distributed around the world. 
	    All you can do is prevent your servers from being damaged, but the service may still be unavailable if the datacenter's routers are being flooded with unwanted traffic.

    \end{frame}

    \begin{frame}
        \frametitle{Protect against breaches}
	    Use a WAF if needed to prevent some common attack types.
	    \begin{alertblock}
		    Remember that a WAF will NEVER replace good (secure) coding practices and will, at best, slow an opportunistic attacker.
	    \end{alertblock}
    \end{frame}

    \begin{frame}
        \frametitle{Protect your servers and infrastructure}
        Use an IDS / IPS to keep scanners and fingerprinting apps away.
    \end{frame}

\end{document}

